Fabricator (:site) do
  name { Faker::Lorem.words(3).join(" ") }
end