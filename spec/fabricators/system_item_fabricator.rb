Fabricator (:system_item) do
  name { Faker::Lorem.words(2).join(" ") }
end