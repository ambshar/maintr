Fabricator (:system_task) do
  task { Faker::Lorem.sentence(3) }
end