Fabricator (:system_item_unit) do
  name { Faker::Lorem.words(1).join(" ") }
end