require 'rails_helper'

RSpec.describe SystemItemUnit, :type => :model do
  it {should have_many(:system_tasks)}
  it {should belong_to(:site)}

  it "has many system tasks via join table" do
    siu = Fabricate(:system_item_unit)
    siust = SystemItemUnitSystemTask.new
    siust.system_item_unit = siu
    st = Fabricate(:system_task)
    siust.system_task = st
    siust.save

    expect(siu.system_tasks.count).to eq(1)

  end
end
