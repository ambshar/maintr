require 'rails_helper'

RSpec.describe SystemItem, :type => :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  it "saves itself" do
    si = Fabricate(:system_item)
    si.save
    expect(SystemItem.last.name).to eq(si.name)
  end

  it {should validate_presence_of(:name)}
  it {should have_many(:system_item_units)}
  it {should have_many(:system_tasks)}
  it {should belong_to(:business_type)}
end
