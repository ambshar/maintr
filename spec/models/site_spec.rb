require 'rails_helper'

RSpec.describe Site, :type => :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  it "saves itself" do
    site = Site.new(name: "mountainview")
    site.save
    expect(Site.last.name).to eq("mountainview")
  end

  it {should validate_presence_of(:name)}
  it {should belong_to(:business_type)}
  it {should have_many(:system_item_units)}
  


end
