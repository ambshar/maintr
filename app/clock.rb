require 'clockwork'
require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)

module Clockwork

  handler do |job, time|
  #  binding.pry
    puts "Running #{job} , at #{time}"
  end
  every(2.minutes, 'send_reminder.job', at: '18:22', :tz => "UTC" ){Reminder.send_reminders}

end