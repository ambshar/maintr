module ViewHelper

  def get_item_object(item)
      #passing the polymorphic object and evaluating the SystemItem or CustomItem

      Kernel.const_get(item.itemable_type).find_by_id(item.itemable_id)
      #binding.pry
  end

  def get_item_unit(reminder)

      ItemUnit.find_by_id(reminder.item_unit_task.item_unit_id)
  end

  def get_task_object(task)
      #passing the polymorphic object and evaluating the name of SystemTask or CustomTask
      Kernel.const_get(task.taskable_type).find_by_id(task.taskable_id)
      #binding.pry
  end
end
