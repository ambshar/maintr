module ApplicationHelper

  def is_active?(link_path)
    "active" if request.fullpath == link_path
  end

  def make_active(site, item)
    #binding.pry
    if request.fullpath == site_items_path(site)
     # binding.pry
      "active" if item == site.items.first
    else
      #binding.pry
      is_active?(site_item_path(site,item))
    end 
  end

  def make_nav_active(link_path) #to keep main nav link active while looking at items in 2nd nav.  example keep Reminders active
    #if (request.fullpath == link_path) || request.check_path_parameters![:controller] == Rails.application.routes.recognize_path(link_path)[:controller]
    
    if (request.fullpath == link_path) || Rails.application.routes.recognize_path(request.fullpath)[:controller] == Rails.application.routes.recognize_path(link_path)[:controller]
      "active" 
      else
      ""
    end 

    
  end

  def make_history_item_nav_active(item)
    my_string = request.fullpath
    test = "item_id=" + item.id.to_s
    if my_string.include? test
      "active"
    else
      ""
    end

  end
  

  def first_site_id
    current_user.sites.first.id
  end

 
  def get_frequency_choice f
    h = Reminder::FREQUENCY
    return h[f]
  end

  
  
end
