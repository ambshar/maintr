$(document).ready(function () { 
     $(document).on('focus', '.datepicker', function () {
          $(this).datepicker({
            minDate: 0,
            dateFormat: 'D, dd M yy',
            altField: ".due_date_alt",
            altFormat: "yy-mm-dd"
          });

      });
     $('[data-toggle="tooltip"]').tooltip({
      'placement': 'top'
     });

     $('#reminder-active').tooltip({
      show: true
     });

});