$(document).ready(function(){
  
  loadCalendar($("#calendar"), "All");

  
   $("#completed-display").dataTable();

  
 
  $("#dashboard-site-select li a").on("click", function(){
    var selText = $(this).text();
    reloadCalendar($("#calendar"), selText);     
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');

    $.ajax({
      type: "GET",
      url: '/dashboard_update',
      data: {site_name: selText},
      dataType: "json",
      success: function(data, textStatus, jqXHR) {
        
        $('#completed-display tbody').empty();
          
        $(function() {
          
            $.each(data.response, function(i, item) {
                    
                var tr = $('<tr>').append(
                $('<td>').text(item.location),
                $('<td>').text(item.unit),
                $('<td>').text(item.task),
                $('<td>').text(item.due_date),
                $('<td>').text(item.completed_on),
                $('<td>').text(item.completed_by)

                ); 
                 $("#completed-display tbody").append(tr);     
                if(item.state == 3) {
                  $(tr).addClass("missed");
                }

              
            }); //.each
          }); //function
      } //success
    }); //.ajax

  }); //dashboard-site-select

 
  

}); //end document

var source = '/rh_events.json?site=all';

function loadCalendar(element, site) {
  
  $(element).fullCalendar({

      events: source
      

  });

}

function reloadCalendar(element, site) {
  var newsource = '/rh_events.json?site=' + site;
  $(element).fullCalendar('removeEventSource', source);
  $(element).fullCalendar('refetchEvents');
  
  $(element).fullCalendar('addEventSource', newsource); 

  
  $(element).fullCalendar('refetchEvents');
  source = newsource
}

