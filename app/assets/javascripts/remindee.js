$(document).ready(function(){

  $(document).bind('ajaxError', 'form#new_remindee', function(event, jqxhr, settings, exception){

    // note: jqxhr.responseJSON undefined, parsing responseText instead
    $(event.data).render_form_errors( $.parseJSON(jqxhr.responseText) );

  });

 
  $("#remindee-select").on("change", function (event){
    //console.log("getting remindee");
  });




  $('#remindee_modal').on('show.bs.modal', function (event) {
    var link = $(event.relatedTarget) // link that triggered the modal
    var siteId = link.data('id') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    //debugger;
    //modal.find('.modal-title').text('New message to ' + recipient)
    //modal.find('.hidden').val(siteId)
    
  });

  $('#remindee_modal').on('hidden.bs.modal', function (event) {
    var modal = $(this);
    modal.removeData('bs.modal');
  
  });



});

function getval(sel){
  var option = $(sel).find("option:selected").text();
   $.ajax({
    type: "GET",
    url: '/sites/remindee_option',
    data: {remindee_name: option},
    dataType: "json",
    success: function(data, textStatus, jqXHR) {
      $("input#remindee_name").attr('value', data.remindee[0].name);
      $("input#remindee_email").attr('value',data.remindee[0].email);
      $("input#remindee_phone").attr('value',data.remindee[0].phone);
    }
  });
 
};

(function($) {

  $.fn.modal_success = function(){
    // close modal
    this.modal('hide');

    // clear form input elements
    // todo/note: handle textarea, select, etc
    this.find('form input[type="text"]').val('');
    //this.find('form select').val('');
    // clear error state
    this.clear_previous_errors();
  };

  $.fn.render_form_errors = function(errors){

    $form = this;
    this.clear_previous_errors();
    model = this.data('model');

    // show error messages in input form-group help-block
    $.each(errors, function(field, messages){
      $input = $('input[name="' + model + '[' + field + ']"]');
      $input.closest('.form-group').addClass('has-error').find('.help-block').html( messages.join(' & ') );
    });

  };

  $.fn.clear_previous_errors = function(){
    $('.form-group.has-error', this).each(function(){
      $('.help-block', $(this)).html('');
      $(this).removeClass('has-error');
    });
  }

}(jQuery));