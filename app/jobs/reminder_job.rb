class ReminderJob < ActiveJob::Base
  queue_as :email

  def perform(reminder, remindee, offset)
    # Do something later
    ReminderMailer.reminder_email(reminder, remindee).deliver_later!(wait: offset.hours)
  end
end
