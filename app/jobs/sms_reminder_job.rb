class SmsReminderJob < ActiveJob::Base
  queue_as :sms

  def perform(reminder, remindee, path)
    # Do something later
    url = ENV['base_url'] + "/confirm/" + path.id.to_s(36)
    test= RestClient.get 'https://api.tropo.com/1.0/sessions', {:params => {
          :action => 'create', 
          :token => ENV["tropo_sms_token"], 
          :numbertodial => remindee.phone, 
          :msg => "#{remindee.name}, '#{reminder.task}' for '#{reminder.item}' due on #{reminder.due_date.strftime("%a, %d %b")}. Click link to complete:  #{url}"}}
  end
end
