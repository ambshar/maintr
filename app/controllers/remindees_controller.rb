class RemindeesController < ApplicationController

  before_action :authenticate_user!
  
  def new
    @site = Site.find params[:site_id]
    @remindee = Remindee.new

  end


  def create
   
    @site = Site.find(params[:remindee][:site_id])
    name = params[:remindee][:name]
    email = params[:remindee][:email]
    phone = params[:remindee][:phone]
    if !(@remindee = current_user.remindees.where(email: email).first)
      #RemindeeSite.create(remindee_id: @remindee.id, site_id: @site.id)
      @remindee = Remindee.new(name: name, email: email, phone: phone, user_id: current_user.id)
    end
    @remindee.sites << @site
    if @remindee.save
      
      respond_to do |format|
        format.html {redirect_to settings_path}
        format.js 
      end
    #else
     # flash[:error] = "Something went wrong"
      #render :back
    end

  end

  def edit
    @remindee = Remindee.find params[:id]
    @site = Site.find params[:site_id]

    respond_to do |format|
      format.html {settings_path}
      format.js 
    end
  end

  def update
    @remindee = Remindee.find(params[:id])
    @site = Site.find params[:remindee][:site_id]

    @remindee.update_attributes(remindee_params)

  end

  def destroy


  end

private

  def remindee_params
    params.require(:remindee).permit(:name, :email, :phone, :user_id)
  end  
  

end
