class Sites::ItemsController < ApplicationController
before_action :authenticate_user!

  def index
    #binding.pry
    @site = get_site(params[:format])  #params[:format] comes from clicking site link_to on settings page
    @items = Item.all.where(site_id: @site.id)
    @item = Item.where(itemable_type: "SystemItem", site_id: @site.id).first
    #binding.pry

  end

  def show
    #binding.pry
    @site = get_site(params[:site_id])
    @items = Item.all.where(site_id: site.id)
    @item = Item.find_by_id(params[:id])
    #binding.pry

  end

  private

  def get_site(id)
    if id == nil
      current_user.sites.first
    else
      Site.find_by_id(id)
      #binding.pry
    end
  end

end
