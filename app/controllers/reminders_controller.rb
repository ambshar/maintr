class RemindersController < ApplicationController
  before_action :authenticate_user!
  include TimeInterval
  helper_method TimeInterval.instance_methods


  def new
    @item_unit_task = ItemUnitTask.find params[:iut_id]
    @site = $current_site
    #@site = Site.find @item_unit_task.item_unit.site_id
    @reminder = Reminder.new
   
  end

 def create
  
  create_update_reminder 
  
   @reminder.remindees << @remindees

  if @reminder.save
    
    
    #@remindees.each do |r|
    #  ReminderRemindee.create(reminder_id: @reminder.id, remindee_id: r.id)
    #end
    create_reminder_history
  end

 end

 def edit
  #binding.pry
  @reminder = Reminder.find params[:id]
  @item_unit_task = ItemUnitTask.find @reminder.item_unit_task_id
  item_unit = @item_unit_task.item_unit
  @site = Site.find item_unit.site_id
  #binding.pry
 end

 def update
  @reminder = Reminder.find params[:id]
  create_update_reminder 
  
   last_reminder_history = @reminder.reminder_histories.last  #if the reminder due date gets changed on editing.  check if task was complete in history
    unless last_reminder_history.complete 
      last_reminder_history.update(due_date: @reminder.due_date) 
      last_reminder_history.remindees = @reminder.remindees.present? ? @reminder.remindees : [] 
    end
 end

   def state
    
    @reminder = Reminder.find params[:reminder_id]

    switch = !@reminder.is_active?
    @reminder.update(is_active?: switch)

   end

 
  def dashboard
    #@pie_chart_data = []
    if current_user.sites.blank?
      redirect_to settings_path

    end

      @reminder_histories = ReminderHistory.includes(:site).where(site: current_user.sites).order("due_date DESC")
   
      dat = ReminderHistory.where(site: current_user.sites).group_by_day(:state, "count", "*")
      #binding.pry
      @reminder_histories.each do |rh|
        #@pie_chart_data << rh.reminder.item

      end
      @display = "all"
   
  end

  def dashboard_update
    site = params[:site_name].downcase
    if site == "all"
      reminder_histories = ReminderHistory.includes(:site).where(site: current_user.sites)
    
    else
      site = Site.find_by_name(site)
      reminder_histories = ReminderHistory.includes(:site).where(site: site)
     
    end
    response = dashboard_data_prep reminder_histories
    respond_to do |format|
      format.json {render json: {response: response}}
    end

  end

 



 private

 def dashboard_data_prep reminder_histories
  response = Array.new
  hash = {}
    
  reminder_histories.each do |rh|
      r = rh.reminder
        hash = {:location => rh.site.name.titleize, :unit => "#{r.item}: #{r.item_unit}", :task => r.task,
          :due_date => rh.due_date.in_time_zone(r.site.time_zone).strftime("%a, %d %b"),:state => rh.state }
          
          hash = hash.merge({:completed_on => (rh.complete ? rh.completion_date.in_time_zone(r.site.time_zone).strftime("%a, %d %b") : "")})  
          hash = hash.merge({:completed_by => (rh.complete ? rh.completed_by.name : "")})
    response << hash

  end
  response
 end

 def create_update_reminder 
  Time.zone = $current_site.time_zone
  iut_id = params[:reminder][:item_unit_task_id]
  @item_unit_task = ItemUnitTask.find iut_id
    
  f = params[:reminder][:frequency] 
  freq = get_freq(f)                          #gets the actual time duration for frequency
  due_date_string = params[:reminder][:due_date] 
  #due = (get_due_date due_date_string).in_time_zone if !due_date_string.blank?   # returns Date
  due = Time.zone.parse(due_date_string) if !due_date_string.blank?   # returns Date


  reminder1_choice = params[:reminder][:reminder1]  #String
  
  if !due_date_string.blank?
  remind_before1 = due - get_remind_before(reminder1_choice)  #Active.Support::TimeWithZone
  remind_before1 = remind_before1.change(Reminder::REMINDER_TIME)   #reminder_time set to 9 AM local
  end

  @remindees = get_remindees params[:reminder][:remindee_ids]

  if @reminder
    @reminder.remindees = @remindees.present? ? @remindees : []
    @reminder.update_attributes(frequency: freq, frequency_choice: f, due_date: due, reminder1_choice: reminder1_choice, reminder1: remind_before1)
    create_confirm_paths @reminder, @reminder.reminder_histories.last    
  else
    @reminder = Reminder.new(frequency: freq, frequency_choice: f, due_date: due, reminder1_choice: reminder1_choice, reminder1: remind_before1, item_unit_task_id: @item_unit_task.id )
  end
 
  
 
 end

 

 def reminder_params
    params.require(:reminder).permit(:frequency,:frequency_choice, :due_date, :reminder1, :reminder2, :item_unit_task_id)
 end 

  def get_due_date due
    #binding.pry
    DateTime.strptime(due, '%Y-%m-%d')  #convert from string to Date class
  end 

 

 def create_reminder_history
    history = ReminderHistory.create(due_date: @reminder.due_date.in_time_zone, reminder_id: @reminder.id, site_id: $current_site.id)
    history.remindees = @reminder.remindees
    @reminder.remindees.each do |remindee|
      ReminderMailer.reminder_created(@reminder, remindee).deliver_later!(wait: 10.seconds)
      send_intro_sms @reminder, remindee
    end
    create_confirm_paths @reminder, history
 end

 def send_intro_sms reminder, remindee
  RestClient.get 'https://api.tropo.com/1.0/sessions', {:params => {
          :action => 'create', 
          :token => ENV["tropo_sms_token"], 
          :numbertodial => remindee.phone, 
          :msg => " #{remindee.name}, you have been added by #{current_user.username} to receive reminders for #{reminder.task} for #{reminder.item}. Check email for details."}}
 end


 def create_confirm_paths reminder, history

  history.confirm_paths.destroy_all if history
  reminder.remindees.each do |remindee|
   path = confirm_reminder_history_path(history.token, remindee_id: remindee.id)
   ConfirmPath.create(confirm_path: path, reminder_history_id: history.id, remindee_id: remindee.id)
  end
 end

 def get_remindees a

    a.reject! {|i| i.empty?}
    return Remindee.where(id: a)
 end
 
 

 
  
end
