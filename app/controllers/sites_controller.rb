class SitesController < ApplicationController
  before_action :authenticate_user!
  around_filter :set_time_zone  #using gem browser_timezone_rails


  before_action only: [:destroy] do
    if !(is_owner_of get_site)
      flash[:error] = "You can't do that"
      redirect_to settings_path
    end
  end

  def new
    tz = ActiveSupport::TimeZone::MAPPING.key(Time.zone.name)  #converting tzinfo timezone names like "America/Los Angeles" to "Pacific Time"
    @site = Site.new(time_zone: tz)
  end

  def create
    
    name= params[:site][:name].downcase
    business_id = params[:site][:business_type_id]
    tz = params[:site][:time_zone]
    @site = Site.new(name: name, business_type_id: business_id, user: current_user, time_zone: tz)

    respond_to do |format|
      if @site.save
        SystemItem.where(business_type_id: business_id).order(name: :asc).each do |si|
          Item.create(itemable: si, site: @site)
        end
        if current_user.remindee_id.blank?
          remindee = Remindee.create(name: current_user.username, email: current_user.email)
          current_user.remindee_id = remindee.id
          current_user.phone = remindee.phone if current_user.phone.blank?
          current_user.save(validate: false)
          RemindeeSite.create(remindee_id: remindee.id, site_id: @site.id)  
        else
          RemindeeSite.create(remindee_id: current_user.remindee_id, site_id: @site.id)
        end
        format.html {redirect_to settings_path}
        format.js
      else
        flash[:error] = "Blank values not accepted"
        redirect_to settings_path
      end
    end
  end

  def destroy
    @site = get_site
    @site.destroy
    respond_to do |format|
      format.html { redirect_to settings_path}
      format.js
      
    end
    

  end

  def remindee_option
    name = params[:remindee_name]
    remindee = current_user.remindees.where(name: name)

    respond_to do |format|
      format.json {render json: {remindee: remindee}}
    end

  end

  private

  def get_site
    Site.find_by_id params[:id]
  end


  def set_time_zone
    old_time_zone = Time.zone
    Time.zone = browser_timezone if browser_timezone.present?
    
    yield
    ensure
      Time.zone = old_time_zone
  end
                                                                                 
  def browser_timezone
    cookies["browser.timezone"]
  end


end