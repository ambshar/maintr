class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
   include ViewHelper
  #protect_from_forgery with: :null_session   #it was :exception  done to work with tropo api
  protect_from_forgery
  skip_before_action :verify_authenticity_token, if: :json_request?

  helper_method  :is_owner_of, :logged_in?, :get_item_unit_task, :get_system_items, 
        :check_system_items_update, :check_system_tasks_update
  before_action :configure_permitted_parameters, if: :devise_controller?


  
   def get_system_items(site, itype)
    Item.where(site: site, itemable_type: itype)
  end                                                                              

  def check_system_items_update  #if new system items have been added to the business_type. system_items are like oven, grill for restaurant business_type
    sys_items = SystemItem.where(business_type_id: $current_site.business_type_id)
    items = get_system_items($current_site, "SystemItem")
    a1 = sys_items.pluck(:id)
    a2 = items.pluck(:itemable_id)
    site = $current_site
    if a1.count - a2.count >0
      new_ids = a1-a2
      new_ids.each do |id|
        si = SystemItem.find_by_id(id)
        Item.create(itemable: si, site: site)
      end    
    end
  end

  def shorten_url(url)
    #binding.pry
    url.to_s(36)
    #binding.pry
  end
  def check_system_tasks_update item  #check if new system tasks have been added to a system item
    
    #binding.pry
    sys_tasks = SystemTask.where(system_item_id: get_item_object(item).id)
    a1 = sys_tasks.pluck(:id)
    item.item_units.each do |iu|
      my_sys_tasks = get_my_system_tasks(iu, "SystemTask")
      a2 = my_sys_tasks.pluck(:taskable_id)
      if a1.count - a2.count >0
        new_tasks_ids = a1-a2
        new_tasks_ids.each do |id|
          st = SystemTask.find_by_id(id)
          task = Task.create(taskable: st)
          ItemUnitTask.create(item_unit_id: iu.id, task_id: task.id)
        end
      end    
    end
  end

  def get_my_system_tasks(item_unit, task_type)
    item_unit_tasks = ItemUnitTask.where(item_unit_id: item_unit.id)
    tasks = []
    item_unit_tasks.each do |iut|
      t = Task.where(id: iut.task_id).first
      tasks << t if t.taskable_type == "SystemTask"
    end
    return Task.where(id: tasks)  #this converts the array 'tasks' to active record relation so that pluck can be used
  end
  

  def get_item_unit_task iu_id, task_id   #passing IDs
   ItemUnitTask.find_by(item_unit_id: iu_id, task_id: task_id)
  end

  

  def is_owner_of site
    current_user.id == site.user_id

  end

  def logged_in?
    !!current_user
    #binding.pry
  end

  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :username
    devise_parameter_sanitizer.for(:account_update)  { |u| u.permit(:username, :email, :phone, :password, :password_confirmation, :remindee_id )}
  end

  def json_request?
    request.format.json?
  end
  
  
end
