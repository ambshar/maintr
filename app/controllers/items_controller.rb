class ItemsController < ApplicationController
before_action :authenticate_user!
before_action :get_site
before_action :check_system_items_update

before_action only: [:index, :show] do
   if !(is_owner_of $current_site)
    flash[:error] = "You can't do that"
    redirect_to settings_path
  end
end

  def index
    @site = $current_site
    if @site  #if a user has a site(location) created else send to settings page
      
      @item = get_system_items(@site, "SystemItem").first   #system_items are entities that user cannot create or destroy
      setup_reminders_page
     
      render 'sites/items/index'
    else
      redirect_to settings_path
    end
  end

  def show
    @item = Item.find_by_id(params[:id])
    setup_reminders_page
    render 'sites/items/index'
  end

private

  def setup_reminders_page
    @site =  $current_site # from clicking site link_to on settings page
    
    @items = Item.where(site_id: @site.id)
    check_system_tasks_update @item if @item.itemable_type == "SystemItem"
      
    @item_unit = ItemUnit.new  #return an instance of item_unit to create a new unit
    @custom_item = CustomItem.new
    @custom_task = CustomTask.new
    @reminder = Reminder.new

  end

  

  def get_site
    
    $current_site = Site.find_by_id(params[:site_id])
    
        #Site.find_by_id(params[:site_id])
  end

  
  
  
  
  
end
