class CustomItemsController < ApplicationController

  def create
    @site = get_site
    @custom_item = CustomItem.new(site_id: @site.id, name: params[:custom_item][:name]) 
    

    if @custom_item.save
      @item = Item.create(itemable: @custom_item, site_id: @site.id)
      @items = Item.where(site_id: @site.id)
      respond_to do |format|
        format.html {redirect_to site_item_path(get_site, @custom_item)}
        format.js 
      end
    else
        flash[:error] = "no blank values"
        render :back
    end


  end

  def destroy
    item = Item.find_by_id params[:id]
    custom_item = get_item_object item
    custom_item.destroy
    redirect_to root_path
  end

private

def get_site
    Site.find_by_id(params[:custom_item][:site_id])
end

end
