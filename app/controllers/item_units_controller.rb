class ItemUnitsController < ApplicationController
  before_action :authenticate_user!

  before_action only: [:destroy] do
    if !(is_owner_of $current_site)
      flash[:error] = "You can't do that"
      redirect_to settings_path
    end
  end
  
  def new
    #this is not used.  instance variable created in items controller
    @item_unit = ItemUnit.new
    @item = Item.find_by_id(params[:item_id])
    #render partial 'new'

  end

  def show
    @item_unit = ItemUnit.find params[:id]
  end

  def create
    #binding.pry
      @item = Item.find_by_id(params[:item_unit][:item_id])
      @site = $current_site   #current site is a global variable
      #@site = Site.find_by_id(@item.site_id)
      @item_unit = ItemUnit.new(name: params[:item_unit][:name], item_id: params[:item_unit][:item_id], site_id: @site.id)
      #binding.pry
      
      respond_to do |format|
        if @item_unit.save
          if @item.itemable_type == 'SystemItem'
            SystemTask.where(system_item_id: get_item_object(@item).id).order(title: :asc).each do |st|
              task = Task.find_by(taskable: st)
              ItemUnitTask.create(item_unit_id: @item_unit.id, task_id: task.id)
            end
          end
          @item.reload
          format.html {redirect_to site_item_path(@site, @item)}
          format.js
        else
          flash[:error] = "Cannot take blank value"
          redirect_to :back
        end
      end
  end

  def destroy
    item_unit = get_item_unit
    @site = $current_site
    @item = item_unit.item
    item_unit.destroy
    #redirect_to site_item_path(site, item)

  end

  
  private

  def get_item_unit
    ItemUnit.find_by_id params[:id]
  end

  def get_site
    Site.find_by_id get_item_unit.site_id
  end

  

end
