class ReminderHistoriesController < ApplicationController
  before_action :authenticate_user!, except: [:sms_back, :confirm, :reminder_updated, :url_redirect]
  include TimeInterval
  before_action :check_system_items_update


  def index
    $current_site = Site.find params[:site_id]
    
      @item = Item.find params[:item_id]
    @items = Item.where(site_id: $current_site.id)
    check_system_tasks_update @item if @item.itemable_type == "SystemItem"

  end

  def confirm
     #shows message when link clicked in email or in sms

    reminder_history = ReminderHistory.find_by_token(params[:id])  #here :id contains the reminder history token
    if reminder_history
      @reminder = Reminder.find reminder_history.reminder_id
      @remindee = Remindee.find params[:remindee_id]
      
      update_reminder_history
      
    else
      render :reminder_updated 

    end
    
  
  end

  def reminder_updated
    #blank action to redirect to reminder_updated view
  end

  def sms_back
    #call back from tropo 
    sessions_object = Tropo::Generator.parse request.env['rack.input'].read
    msg = sessions_object[:session][:parameters][:msg]
    number_to_dial = sessions_object[:session][:parameters][:numbertodial]


    tropo = Tropo::Generator.new do
      message({
      :to => number_to_dial,
      :channel => 'TEXT',
      :network => 'SMS'}) do
      say :value => "#{msg}"
      end
    end
     
    tropo.response
    respond_to do |format|
      format.json {render json: tropo.response}
    end

  end

  def complete
    #when Done is clicked by the user

  @reminder = Reminder.find params[:id]   #the :id is the reminder_id 
  @remindee = Remindee.find current_user.remindee_id
  update_reminder_history   #updates current reminder history
  
  @item_unit_task = ItemUnitTask.find @reminder.item_unit_task_id
    respond_to do |format|
      format.js {render 'reminders/update'}
    end
  end

  def url_redirect
    #action when link in sms is clicked.  the actual path is extracted and redirected to action #confirm
    
    token = params[:id]
  
    path = ConfirmPath.find(token.to_i(36))
    redirect_to path.confirm_path

  end

  

  def events
    #preps and sends reminder histories data to be populated in calendar in the dashboard
    #aiu = Hash.new

    sites = params[:site].downcase
    if sites == "all"
        sites = current_user.sites
    else
      sites = Site.where(name: sites)
    end
    reminder_histories = ReminderHistory.includes(:site).where(site: sites)

   
    @rh_events = reminder_histories.map{|rh| rh.complete ? {title: get_title(rh), description: get_description(rh), 
                start: rh.due_date, timezone: rh.site.time_zone, allDay: true, 
                className: "calendar-done"} : {title: get_title(rh), description: get_description(rh), 
                start: rh.due_date, timezone: rh.site.time_zone, allDay: true, className: "calendar-missed"} }
    

              
    respond_to do |format|
      format.json {render json: @rh_events}
    end

  end

  private

  def get_title rh
    iu = get_item_unit rh.reminder

    item = get_item_object iu.item

    return item.name + ": " + iu.name

  end

  def get_description rh
    task = get_task_object rh.reminder.item_unit_task.task
    return task.title
  end


  def index_due_date
    increment = (@reminder.frequency.to_i/86400).days
  due = @reminder.due_date +  increment  #calculate number of days for the frequency.  86400 secs in one day
  reminder1 = (@reminder.reminder1 + increment).change(Reminder::REMINDER_TIME)

  @reminder.update_attributes(due_date: due, reminder1: reminder1)
 end

 def update_reminder_history
  #remindee id will have to be revisited once email and sms acknowledgement is working
  #the penultimate history record is updated if todays date is before the reminder_before date for the current due date.  
  #Provided the penultimate history is not complete
  site = get_site @reminder
  Time.zone = site.time_zone
  confirmation_time = Time.zone.now
  state = get_completion_state
  if state == 1
    h = @reminder.reminder_histories.last
    h.update(completion_date: confirmation_time, complete: true, remindee_id: @remindee.id, state: state, token: nil)
    h.confirm_paths.destroy_all
  else
    h = @reminder.reminder_histories[-2]
    h.update(completion_date: confirmation_time, complete: true, remindee_id: @remindee.id, state: state,  token: nil)
    h.confirm_paths.destroy_all
  end

    
  index_due_date            #changes the next due date for the reminder
  create_reminder_history   #create new reminder history for the new due date


 end

 def get_completion_state
  return 1 if @reminder.reminder_histories.count == 1  #for the first history record. as soon as someone clicks, DONE
  penultimate = @reminder.reminder_histories[-2]  #get the second to last reminder history record
  if penultimate && penultimate.complete  #if penultimate reminder is complete then current reminder is done
    return 1
  else                                   #penultimate reminder is not completed then check the dates in relation to today
    due = @reminder.due_date   #due date of current reminder history
    reminder1 = @reminder.reminder1  #remind before date of current reminder history
    if Time.zone.today < reminder1   #if todays date is before the remind before date then punultimate reminder
      return 2                       #is late-complete... hence state of 2
    else
      return 1                       #if todays date is after the remind before date i.e. reminder has been sent
    end                              #for the current reminder history, then mark the current one as complete.  
  end                                 #the penultimate reminder history will have a state of 3 i.e. incomplete

 end

 def get_site reminder
  iut = ItemUnitTask.find reminder.item_unit_task_id
  Site.find_by_id(ItemUnit.find_by_id(iut.item_unit_id).site_id)
 end

 def create_reminder_history

  history = ReminderHistory.create(due_date: @reminder.due_date.in_time_zone, reminder_id: @reminder.id, site_id: $current_site.id)
  history.remindees = @reminder.reload.remindees
  @reminder.remindees.each do |remindee|
    path = confirm_reminder_history_path(history.token, remindee_id: remindee.id)
    ConfirmPath.create(confirm_path: path, reminder_history_id: history.id, remindee_id: remindee.id)
  end

 end

end
