class PagesController < ApplicationController

  def front
    if current_user
      if !current_user.sites.blank?
        $current_site = current_user.sites.first
        redirect_to dashboard_path

      else
        redirect_to settings_path
      end
    end
  end

  
end
