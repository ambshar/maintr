class RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!
  after_action only: [:create] do
    create_remindee_entry
    user_created
  end
  

  def edit_password
    @user = current_user
    render 'devise/registrations/edit_password'
  end

  def update_password
   
    current_user.update_with_password(account_update_params)
    flash[:success] = "Password changed" if !params[:password].blank?
    render 'devise/registrations/update_password'
    #redirect_to site_items_path
  end
  protected

  def update_resource(resource, params)   #this is from devise wiki
    resource.update_without_password(params)
  end

  private
  def create_remindee_entry
      remindee=Remindee.create(name: resource.username, email: resource.email, phone: resource.phone, user_id: resource.id)
      resource.update_without_password(remindee_id: remindee.id)
  end
  
  def user_created
    UserMailer.user_created(resource).deliver_now!
  end


end