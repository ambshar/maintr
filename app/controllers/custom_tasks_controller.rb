class CustomTasksController < ApplicationController
  before_action :authenticate_user!


  def new
    
    @site = $current_site
    #@site= Site.find_by_id(params[:site_id])
    @item = Item.find_by_id(params[:item_id])
    #binding.pry
    if @item.item_units.empty?
      flash[:error] = "Add a #{get_item_object(@item).name} before creating a task"
      redirect_to :back
    else
      @custom_task = CustomTask.new
    end
  end

  def create
    #binding.pry
    @item_unit_ids = get_item_units 
    title = params[:custom_task][:title]
    description = params[:custom_task][:description]
    #binding.pry
    if @item_unit_ids.blank? || title.blank?
      flash[:error] = "Something is wrong.  Blank values not accepted."
      redirect_to :back
    else 
      @site = $current_site
      #@site = get_site @item_unit_ids
      item = get_item @item_unit_ids
      #binding.pry
      @custom_task = CustomTask.new(title: title, description: description)
      if @custom_task.save
        @task = Task.create(taskable: @custom_task)
        @item_unit_ids.each do |iu_id|
          ItemUnitTask.create(item_unit_id: iu_id, task_id: @task.id)
        end
        @reminder = Reminder.new
        respond_to do |format|

          format.html {redirect_to site_item_path(@site, item)}
          format.js

        end
      end
    end


  end

  def destroy

    item_unit = ItemUnit.find_by_id params[:item_unit]
    @site = $current_site
    #@site = Site.find_by_id item_unit.site_id
    @item = Item.find_by_id item_unit.item_id
    custom_task = CustomTask.find_by_id params[:id]
    task = Task.find_by taskable: custom_task
    item_unit_task = ItemUnitTask.find_by(item_unit_id: item_unit.id, task_id: task.id)
    item_unit_task.destroy

    if ItemUnitTask.find_by(task_id: task.id).blank?
      Task.find(task.id).destroy
      custom_task.destroy
    end
    
    respond_to do |format|

      format.html {redirect_to site_item_path(Site.find(item_unit.site_id), Item.find(item_unit.item_id))}
      format.js

    end
  end

  private

  def get_item_units

    a = params[:custom_task][:unit]

    a.reject! {|i| i.empty?}
  end
  def get_site iu
    site_id = ItemUnit.find_by_id(iu[0]).site_id
    Site.find_by_id site_id
  end
  def get_item iu
    ItemUnit.find_by_id(iu[0]).item
  end
end
