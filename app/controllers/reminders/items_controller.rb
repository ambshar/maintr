class Reminders::ItemsController < ApplicationController
  before_action :authenticate_user!


  def index
    #come here when Reminder on the nav bar is clicked   

    @site = current_user.sites.first 
    @items = Item.all.where(site_id: @site.id)
    @item = Item.where(itemable_type: "SystemItem", site_id: @site.id).first
    #binding.pry
    render 'sites/items/index'
  end


end
