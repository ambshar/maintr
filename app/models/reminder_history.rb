class ReminderHistory < ActiveRecord::Base
  belongs_to :reminder
  belongs_to :completed_by, foreign_key: 'remindee_id', class_name: 'Remindee'
  belongs_to :site
  
  has_many :reminder_history_remindees, dependent: :destroy
  has_many :remindees, through: :reminder_history_remindees
  
  has_many :confirm_paths, dependent: :destroy

  has_secure_token

 
end
