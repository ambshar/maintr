class ItemUnit < ActiveRecord::Base

  belongs_to :item
  
  belongs_to :site
  has_many :item_unit_tasks, dependent: :destroy
  has_many :tasks, through: :item_unit_tasks

end
