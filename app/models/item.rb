class Item < ActiveRecord::Base
  belongs_to :itemable, polymorphic: true
  belongs_to :site
  has_many :item_units, dependent: :destroy



end
