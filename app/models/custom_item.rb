class CustomItem < ActiveRecord::Base
has_one :item, as: :itemable, dependent: :destroy


validates_presence_of :name

end