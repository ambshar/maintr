class ItemUnitTask < ActiveRecord::Base
  belongs_to :item_unit
  belongs_to :task
  has_one :reminder, dependent: :destroy
end
