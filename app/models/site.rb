class Site < ActiveRecord::Base
  has_many :items, dependent: :destroy
  belongs_to :business_type
  has_many :item_units
  has_many :reminder_histories
  has_many :remindee_sites, dependent: :destroy
  has_many :remindees, through: :remindee_sites

  validates_presence_of :name, :business_type_id
  belongs_to :user
end
