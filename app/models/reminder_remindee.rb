class ReminderRemindee < ActiveRecord::Base
  belongs_to :reminder
  belongs_to :remindee
end
