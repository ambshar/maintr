class CustomTask < ActiveRecord::Base

has_one :task, as: :taskable, dependent: :destroy
validates_presence_of :title
attr_accessor :unit

end
