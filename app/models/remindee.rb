class Remindee < ActiveRecord::Base

  phony_normalize :phone, :default_country_code => 'US'
  
  has_many :remindee_sites, dependent: :destroy
  has_many :sites, through: :remindee_sites

  has_many :reminder_remindees, dependent: :destroy
  has_many :reminders, through: :reminder_remindees
  
  belongs_to :user

  has_many :reminder_history_remindees, dependent: :destroy
  has_many :reminder_histories, through: :reminder_history_remindees

  has_many :confirm_paths, dependent: :destroy  #this is to save confirmation paths and shorten the url sent in sms
  
  validates :name, presence: true
  validates_email_format_of :email, presence: true, :message => 'is not looking good'
  validates_plausible_phone :phone
  validates_uniqueness_of :email

  attr_accessor :remindee_option   #this is for the dropdown in the 'add recipients' form displaying existing remindees
end
