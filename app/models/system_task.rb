class SystemTask < ActiveRecord::Base
  belongs_to :system_item
  
  has_many :system_item_unit_system_tasks
  has_many :system_item_units, through: :system_item_unit_system_tasks

  has_one :task, as: :taskable
  validates_presence_of :title
  validates_presence_of :system_item

  after_save :create_task


  private

  def create_task
    Task.create(taskable: self)
  end

  
end
