class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  has_many :sites, dependent: :destroy
  has_many :remindees, dependent: :destroy
  validates_presence_of :password, :password_confirmation, on: :create
  validates_presence_of :username, :email
  validates_uniqueness_of :email




end
