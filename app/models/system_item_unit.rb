class SystemItemUnit < ActiveRecord::Base
  has_many :system_item_unit_system_tasks
  has_many :system_tasks, through: :system_item_unit_system_tasks

  #has_one :item_unit, as: :item_unitable
end
