class BusinessType < ActiveRecord::Base
  has_many :sites
  has_many :system_items
  validates_presence_of :name
end
