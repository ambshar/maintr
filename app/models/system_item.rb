class SystemItem < ActiveRecord::Base

  belongs_to :business_type
  has_many :system_item_units
  has_many :system_tasks, dependent: :destroy
  has_one :item, as: :itemable

  validates_presence_of :name
end
