class Task < ActiveRecord::Base
  belongs_to :taskable, polymorphic: true, dependent: :destroy

  has_many :item_unit_tasks, dependent: :destroy
  has_many :item_units, through: :item_unit_tasks
end
