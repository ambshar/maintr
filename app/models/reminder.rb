class Reminder < ActiveRecord::Base
  include TimeInterval
  include ViewHelper
FREQUENCY = {
  1 => "every day",
  2 => "once a week",
  3 => "every 2 weeks",
  4 => " once a month",
  5 => " 2 months",
  6 => "3 months",
  7 => "6 months",
  8 => "once a year"
}

REMIND_BEFORE = {
  1 => "every day",
  2 => "a day before",
  3 => "2 days before",
  4 => "one week before",
  5 => "2 weeks before",
  6 => "3 weeks before",
  7 => "one month before"
} 
  
  REMINDER_TIME = {hour: 9, min: 0, sec: 0}

  belongs_to :item_unit_task

  has_many :reminder_remindees, dependent: :destroy
  has_many :remindees, through: :reminder_remindees

  has_many :reminder_histories, dependent: :destroy

  validates :due_date,  presence: true
  #validates_presence_of :remindees, :message => "Atleast one notification recipient is to be selected"
  validate :remindees_presence 
  validate :remind_before_less_than_frequency
  
  def self.send_reminders
    d = Date.today.in_time_zone("UTC")
    @reminders = Reminder.where(reminder1: d.beginning_of_day..d.end_of_day, is_active?: true).all
    @reminders.each do |r|
       
      r.remindees.each do |rem|
        offset = (r.reminder1 - d.beginning_of_day)/3600
        #offset = (r.reminder1 - Time.now.utc)/3600
        ReminderJob.perform_later(r, rem, offset.to_f)  #sends emails
       
        if rem.phone
          rh = r.reminder_histories.last
          path = ConfirmPath.where(remindee_id: rem.id, reminder_history_id: rh.id ).first
          SmsReminderJob.set(wait: offset.to_f.hours).perform_later(r, rem, path)  #sends sms
          

        end
      end
    end
  end

  def self.due_date_index

  end

  
  def task
    get_task_object(self.item_unit_task.task).title
  end

  def description
    get_task_object(self.item_unit_task.task).description
  end

  def site
    self.reminder_histories.first.site if self
  end
  def item_unit
    self.item_unit_task.item_unit.name
  end


  def item
    get_item_object(self.item_unit_task.item_unit.item).name
  end



  private
  def remind_before_less_than_frequency
    freq = self.get_freq(frequency_choice)
    remind1 = self.get_remind_before(reminder1_choice)

    errors.add(:frequency, "should be greater than 'Send reminder' time interval" ) if freq < remind1

  end

  def remindees_presence
    errors.add(:base, "Atleast one notification recipient should be selected" ) if self.remindees.blank?


  end

  
 
  
end
