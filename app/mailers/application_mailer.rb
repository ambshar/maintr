class ApplicationMailer < ActionMailer::Base
 include ViewHelper
 helper ViewHelper
  default from: "no-reply@500gallons.com"
  layout 'mailer'
end
