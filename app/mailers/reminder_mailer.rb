class ReminderMailer < ApplicationMailer
  

  def reminder_created(reminder, remindee)
    @reminder = reminder

    @remindee = remindee

    mail to: remindee.email, subject: "You have been added"

  end

  def reminder_email(reminder, remindee)
    @reminder = reminder

    @remindee = remindee

    mail to: remindee.email, subject: "Your task reminder"

  end


end
