# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150630185300) do

  create_table "business_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "confirm_paths", force: :cascade do |t|
    t.string   "confirm_path"
    t.integer  "reminder_history_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "remindee_id"
  end

  create_table "custom_items", force: :cascade do |t|
    t.string   "name"
    t.integer  "site_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "custom_tasks", force: :cascade do |t|
    t.text     "title"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "item_unit_tasks", force: :cascade do |t|
    t.integer  "item_unit_id"
    t.integer  "task_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "item_units", force: :cascade do |t|
    t.integer  "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.integer  "site_id"
  end

  create_table "items", force: :cascade do |t|
    t.integer  "itemable_id"
    t.string   "itemable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "site_id"
  end

  create_table "remindee_sites", force: :cascade do |t|
    t.integer  "remindee_id"
    t.integer  "site_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "remindees", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "reminder_histories", force: :cascade do |t|
    t.datetime "completion_date"
    t.datetime "due_date"
    t.boolean  "complete",        default: false
    t.integer  "remindee_id"
    t.integer  "reminder_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "state",           default: 3
    t.string   "token"
    t.integer  "site_id"
  end

  create_table "reminder_history_remindees", force: :cascade do |t|
    t.integer "reminder_history_id"
    t.integer "remindee_id"
  end

  create_table "reminder_remindees", force: :cascade do |t|
    t.integer  "reminder_id"
    t.integer  "remindee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "reminders", force: :cascade do |t|
    t.string   "frequency"
    t.datetime "due_date"
    t.datetime "reminder1"
    t.datetime "reminder2"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "item_unit_task_id"
    t.string   "frequency_choice"
    t.boolean  "is_active?",        default: true
    t.string   "reminder1_choice"
  end

  add_index "reminders", ["reminder1"], name: "index_reminders_on_reminder1"

  create_table "sites", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "business_type_id"
    t.integer  "user_id"
    t.string   "time_zone"
  end

  create_table "system_item_unit_system_tasks", force: :cascade do |t|
    t.integer  "system_item_unit_id"
    t.integer  "system_task_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_item_units", force: :cascade do |t|
    t.string   "name"
    t.integer  "system_item_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "system_items", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "business_type_id"
  end

  create_table "system_tasks", force: :cascade do |t|
    t.text     "title"
    t.integer  "system_item_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.text     "description"
  end

  create_table "tasks", force: :cascade do |t|
    t.integer  "taskable_id"
    t.string   "taskable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "username",               default: ""
    t.boolean  "admin",                  default: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "remindee_id"
    t.string   "phone"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
