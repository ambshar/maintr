class AddFreqChoiceAndIsActiveToReminders < ActiveRecord::Migration
  def change
    add_column :reminders, :frequency_choice, :string
    add_column :reminders, :is_active?, :boolean, default: true
  end
end
