class MakeItemUnitsNonPolymorphic < ActiveRecord::Migration
  def change
    remove_column :item_units, :item_unitable_id
    remove_column :item_units, :item_unitable_type
    add_column :item_units, :name, :string

  end
end
