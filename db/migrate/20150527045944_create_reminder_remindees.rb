class CreateReminderRemindees < ActiveRecord::Migration
  def change
    create_table :reminder_remindees do |t|
      t.integer :reminder_id
      t.integer :remindee_id
      t.timestamps null: false
    end
  end
end
