class CreateReminderHistoryRemindees < ActiveRecord::Migration
  def change
    create_table :reminder_history_remindees do |t|
      t.integer :reminder_history_id
      t.integer :remindee_id
    end
  end
end
