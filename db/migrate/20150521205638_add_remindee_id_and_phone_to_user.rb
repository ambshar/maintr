class AddRemindeeIdAndPhoneToUser < ActiveRecord::Migration
  def change
    add_column :users, :remindee_id, :integer
    add_column :users, :phone, :string
  end
end
