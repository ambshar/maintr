class CreateRemindeeSites < ActiveRecord::Migration
  def change
    create_table :remindee_sites do |t|
      t.integer :remindee_id
      t.integer :site_id
      t.timestamps null: false
    end
  end
end
