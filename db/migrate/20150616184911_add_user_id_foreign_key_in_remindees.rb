class AddUserIdForeignKeyInRemindees < ActiveRecord::Migration
  def change
    add_column :remindees, :user_id, :integer
  end
end
