class AddDescriptionAndRenameContentForSystemTasks < ActiveRecord::Migration
  def change
    add_column :system_tasks, :description, :text
    rename_column :system_tasks, :content, :title
  end
end
