class CreateItemUnitTasks < ActiveRecord::Migration
  def change
    create_table :item_unit_tasks do |t|
      t.integer :item_unit_id
      t.integer :task_id
      t.timestamps null: false
    end
  end
end
