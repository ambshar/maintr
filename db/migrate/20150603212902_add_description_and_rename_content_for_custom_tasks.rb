class AddDescriptionAndRenameContentForCustomTasks < ActiveRecord::Migration
  def change
    add_column :custom_tasks, :description, :text
    rename_column :custom_tasks, :content, :title
  end
end
