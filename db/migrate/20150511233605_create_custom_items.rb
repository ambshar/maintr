class CreateCustomItems < ActiveRecord::Migration
  def change
    create_table :custom_items do |t|
      t.string :name
      t.integer :site_id
      t.timestamps null: false
    end
  end
end
