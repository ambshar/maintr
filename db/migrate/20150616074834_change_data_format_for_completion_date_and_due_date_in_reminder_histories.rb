class ChangeDataFormatForCompletionDateAndDueDateInReminderHistories < ActiveRecord::Migration
  def change
    change_column :reminder_histories, :due_date, :datetime
    change_column :reminder_histories, :completion_date, :datetime
  end
end
