class CreateReminderHistories < ActiveRecord::Migration
  def change
    create_table :reminder_histories do |t|
      t.date :completion_date
      t.date :due_date
      t.boolean :complete, default: false

      t.integer :remindee_id
      t.integer :reminder_id


      t.timestamps null: false
    end
  end
end
