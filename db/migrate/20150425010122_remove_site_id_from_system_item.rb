class RemoveSiteIdFromSystemItem < ActiveRecord::Migration
  def change
    remove_column :system_items, :site_id
  end
end
