class CreateCustomTasks < ActiveRecord::Migration
  def change
    create_table :custom_tasks do |t|
      t.text :content
      t.timestamps null: false
    end
  end
end
