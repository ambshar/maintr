class AddSiteIdToSystemItem < ActiveRecord::Migration
  def change
    add_column :system_items, :site_id, :integer
  end
end
