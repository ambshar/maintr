class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :taskable, polymorphic: true
      t.integer :item_unit_id
      t.timestamps null: false
    end
  end
end
