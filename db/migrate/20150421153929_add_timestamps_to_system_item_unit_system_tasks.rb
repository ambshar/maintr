class AddTimestampsToSystemItemUnitSystemTasks < ActiveRecord::Migration
  def change
    add_column :system_item_unit_system_tasks, :created_at, :datetime
    add_column :system_item_unit_system_tasks, :updated_at, :datetime
  end
end
