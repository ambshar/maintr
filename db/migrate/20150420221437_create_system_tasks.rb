class CreateSystemTasks < ActiveRecord::Migration
  def change
    create_table :system_tasks do |t|
      t.text :task
      t.integer :system_item_id

      t.timestamps null: false
    end
  end
end
