class AddSiteIdToItemUnit < ActiveRecord::Migration
  def change
    add_column :item_units, :site_id, :integer
  end
end
