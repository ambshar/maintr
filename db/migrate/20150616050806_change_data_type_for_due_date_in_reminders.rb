class ChangeDataTypeForDueDateInReminders < ActiveRecord::Migration
  def change
    change_column :reminders, :due_date, :datetime
  end
end
