class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :itemable, polymorphic: true
      t.timestamps null: false
    end
  end
end
