class RemoveSiteIdFromRemindees < ActiveRecord::Migration
  def change
    remove_column :remindees, :site_id
  end
end
