class AddBusinessTypeIdToSites < ActiveRecord::Migration
  def change
    add_column :sites, :business_type_id, :integer
  end
end
