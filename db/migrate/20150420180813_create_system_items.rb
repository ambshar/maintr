class CreateSystemItems < ActiveRecord::Migration
  def change
    create_table :system_items do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
