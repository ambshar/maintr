class CreateConfirmPathsTable < ActiveRecord::Migration
  def change
    create_table :confirm_paths do |t|
      t.string :confirm_path
      t.integer :reminder_history_id
      t.timestamps null: false
    end
  end
end
