class CreateItemUnits < ActiveRecord::Migration
  def change
    create_table :item_units do |t|
      t.references :item_unitable, polymorphic: true
      t.integer :item_id
      t.timestamps null: false
    end
  end
end
