class RemoveSiteIdFromSystemitemunits < ActiveRecord::Migration
  def change
    remove_column :system_item_units, :site_id
  end
end
