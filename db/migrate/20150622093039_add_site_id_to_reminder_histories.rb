class AddSiteIdToReminderHistories < ActiveRecord::Migration
  def change
    add_column :reminder_histories, :site_id, :integer
  end
end
