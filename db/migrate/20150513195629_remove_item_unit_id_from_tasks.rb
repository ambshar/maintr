class RemoveItemUnitIdFromTasks < ActiveRecord::Migration
  def change
    remove_column :tasks, :item_unit_id
  end
end
