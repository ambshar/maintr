class AddSiteIdToRemindees < ActiveRecord::Migration
  def change
     add_column :remindees, :site_id, :integer
  end
end
