class AddTokenToReminderHistories < ActiveRecord::Migration
  def change
    add_column :reminder_histories, :token, :string
  end
end
