class RenameSystemTaskColumnTaskToContent < ActiveRecord::Migration
  def change
    rename_column :system_tasks, :task, :content
  end
end
