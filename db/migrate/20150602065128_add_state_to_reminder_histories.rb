class AddStateToReminderHistories < ActiveRecord::Migration
  def change
    add_column :reminder_histories, :state, :integer, default: 3
  end
end
