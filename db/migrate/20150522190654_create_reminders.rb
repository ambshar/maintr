class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|

      t.string :frequency
      t.date :due_date
      t.datetime :reminder1
      t.datetime :reminder2
      

      t.timestamps null: false
    end
  end
end
