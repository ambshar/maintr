class AddSiteIdToItem < ActiveRecord::Migration
  def change
    add_column :items, :site_id, :integer
  end
end
