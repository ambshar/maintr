class AddRemindeeIdToConfirmPaths < ActiveRecord::Migration
  def change
    add_column :confirm_paths, :remindee_id, :integer
  end
end
