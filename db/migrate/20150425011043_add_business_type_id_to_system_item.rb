class AddBusinessTypeIdToSystemItem < ActiveRecord::Migration
  def change
    add_column :system_items, :business_type_id, :integer
  end
end
