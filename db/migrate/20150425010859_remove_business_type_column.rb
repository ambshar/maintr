class RemoveBusinessTypeColumn < ActiveRecord::Migration
  def change
    remove_column :sites, :business_type
  end
end
