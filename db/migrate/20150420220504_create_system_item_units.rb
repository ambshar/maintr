class CreateSystemItemUnits < ActiveRecord::Migration
  def change
    create_table :system_item_units do |t|

      t.string :name
      t.integer :system_item_id

      t.timestamps null: false
    end
  end
end
