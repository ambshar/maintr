class AddItemUnitTaskIdToReminders < ActiveRecord::Migration
  def change
    add_column :reminders, :item_unit_task_id, :integer
  end
end
