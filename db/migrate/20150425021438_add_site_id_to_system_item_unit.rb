class AddSiteIdToSystemItemUnit < ActiveRecord::Migration
  def change
    add_column :system_item_units, :site_id, :integer
  end
end
