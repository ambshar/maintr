class CreateSystemItemUnitSystemTasks < ActiveRecord::Migration
  def change
    create_table :system_item_unit_system_tasks do |t|
      t.integer :system_item_unit_id
      t.integer :system_task_id
    end
  end
end
