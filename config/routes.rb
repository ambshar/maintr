Maintr::Application.routes.draw do

  devise_for :users, controllers: {registrations: 'registrations'}
  as :user do
    get 'users/edit_password' => 'registrations#edit_password', :as => 'edit_user_password_registration'    
    patch 'users/:id' => 'registrations#update_password', :as => 'user_password_registration'            
  end
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  

  root to: 'pages#front'
  get '/settings', to: 'settings#show'
  get '/dashboard', to: 'reminders#dashboard'
  match "/delayed_job" => DelayedJobWeb, :anchor => false, via: [:get, :post]
  get 'sites/remindee_option', to: 'sites#remindee_option'  #for the ajax request from sites form
  get '/confirm/:id', to: 'reminder_histories#url_redirect'
  match "/sms_back.json", to: 'reminder_histories#sms_back', via: [:get, :post], defaults: {format: 'json'}
  get '/dashboard_update', to: 'reminders#dashboard_update'  #for ajax call to update dashboard
  namespace :reminders do
    resources :items, only: [:index]
  end

  resources :sites do
    resources :items, only: [:index, :show, :destroy]

  end

  resources :item_units

  resources :custom_items, only: [:new, :create, :destroy]
  resources :custom_tasks, only: [:new, :create, :destroy]
  resources :remindees, only: [:new, :create, :edit, :update, :destroy]
  resources :reminders, only: [ :new, :create, :edit, :update] do
    post :state
  end

  resources :reminder_histories, only: [:index, :show], as: :histories
  get '/rh_events.json', to: 'reminder_histories#events', defaults: {format: 'json'}  #to pass reminder history data to calendar
  resources :reminder_histories,  only: [:show] do
    member do
      post :complete
      get :reminder_updated
      get :confirm
    end
  end


  
  
  devise_scope :user do
    get 'users/sign_out', to: 'devise/sessions#destroy'
  end

  #resources :sites, only: [:create]
  

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
