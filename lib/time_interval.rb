module TimeInterval
  extend ActiveSupport::Concern

  def get_freq(freq)  #selecting how often the task is to be performed. called from model, controller Reminder

    case freq.to_i
      when 1
        1.day
      when 2
        1.week
      when 3
        2.weeks
      when 4
        1.month
      when 5
        2.months
      when 6
        3.months
      when 7
        6.months
      when 8
        1.year
    end

 end
  
  def get_remind_before(selection)  
 #setting number of days before the due date to send reminder 

    case selection.to_i
      when 1
        0.day
      when 2
        1.day
      when 3
        2.days
      when 4
        1.week
      when 5
        2.weeks
      when 6
        3.weeks
      when 7
        1.month
    end

  end

end